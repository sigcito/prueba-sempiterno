import { Controller } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @MessagePattern({ cmd: 'sempiterno' })
  async getSempiternoName(name: string): Promise<string> {
    return `Este es un mensaje mostrando un nombre: ${name}`;
  }
}
