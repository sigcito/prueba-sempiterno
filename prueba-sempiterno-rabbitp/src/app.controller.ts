import { Body, Controller, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/sempiterno')
  async sendSempiternoData(@Body() body: any) {
    return this.appService.getSempiternoName(body.name);
  }
}
