import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class AppService {
  constructor(@Inject('SEMPITERNO_SERVICE') private client: ClientProxy) {}

  async getSempiternoName(name: string) {
    const message = this.client.send({ cmd: 'sempiterno' }, name);
    return message;
  }
}
