import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { IJwtPayload } from '../../domain/jwt-payload.interface';
import { UnauthorizedException, Injectable, Inject } from '@nestjs/common';
import {
  UserRepository,
  USER_REPOSITORY,
} from '../../../../backoffice/user/domain/user.repository';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    @Inject(USER_REPOSITORY)
    private readonly _authRepository: UserRepository,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey:process.env.JWT_SECRET,
    });
  }

  async validate(payload: IJwtPayload) {
    const { username } = payload;
    const user = await this._authRepository.validarUsuaro(username);

    if (!user) {
      throw new UnauthorizedException();
    }

    return payload;
  }
}
