import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
  Inject,
  ConflictException,
} from '@nestjs/common';

import { JwtService } from '@nestjs/jwt';

import * as bcrypt from 'bcrypt';

import { User } from '../../../shared/domain/user.entity';
import { IJwtPayload } from '../domain/jwt-payload.interface';
import { SigninDto, SignupDto } from '../domain';
import {
  UserRepository,
  USER_REPOSITORY,
} from '../../../backoffice/user/domain/user.repository';
import { STATUS_DELETE } from '../../../shared/constants';
import { UtilsService } from 'src/shared/services/util.service';

@Injectable()
export class AuthService {
  constructor(
    @Inject(USER_REPOSITORY)
    private readonly _authRepository: UserRepository,
    private readonly _jwtService: JwtService,
    private readonly _utilsService: UtilsService,
  ) {}

  async signup(signupDto: SignupDto): Promise<User> {
    const {
      username,
      email,
      password,
      nombre,
      apellidoPat,
      apellidoMat,
      telefono,
    } = signupDto;
    const userExists = await this._authRepository.userExists(username, email);

    if (userExists) {
      throw new ConflictException('username or email already exists');
    }

    const user = new User();
    user.username = username;
    user.email = email;
    user.nombre = nombre;
    user.apellidoPat = apellidoPat;
    user.apellidoMat = apellidoMat;
    user.telefono = telefono;

    const saltOrRounds = await bcrypt.genSalt();
    user.password = await bcrypt.hash(password, saltOrRounds);

    return this._authRepository.save(user);
  }

  async signin(signinDto: SigninDto): Promise<{ token: string }> {
    const { username, password } = signinDto;

    const user: User = await this._authRepository.findOne(username);

    if (!user) {
      throw new NotFoundException('El usuario no existe');
    }
    if (user.status == STATUS_DELETE) {
      throw new NotFoundException('El usuario esta inactivo');
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      throw new UnauthorizedException('invalid credentials');
    }

    const payload: IJwtPayload = {
      id: user.id,
      email: user.email,
      username: user.username,
    };

    const token = this._jwtService.sign(payload);

    return { token };
  }
}
