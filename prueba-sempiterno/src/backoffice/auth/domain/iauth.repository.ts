import { User } from '../../../shared/domain/user.entity';
import { SignupDto } from './signup.dto';
export const AUTH_REPOSITORI = 'AUTH_REPOSITORI';
export interface IAuthRepository {
  signup(signupDto: SignupDto): Promise<User>;
}
