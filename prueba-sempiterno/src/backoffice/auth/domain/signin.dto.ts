import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SigninDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'User name required', example: 'alejandro' })
  username: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'User password required', example: '123456' })
  password: string;
}
