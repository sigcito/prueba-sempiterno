import { IsDate, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class SignupDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'User name', example: 'alejandro' })
  // @ApiProperty({ example: "usename", description: 'User name' })
  username: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'User email', example: 'sieg_1594@hotmail.com' })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'User password', example: '123456' })
  password: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'Name of the user', example: 'Alejandro' })
  nombre: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'Apellido of the user', example: 'Gonzalez' })
  apellidoPat: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: 'Apellido of the user', example: 'Carrero' })
  apellidoMat: string;

  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  @ApiProperty({ description: 'BirthDate of the user', example: new Date() })
  fechaNac: Date;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'Phone of the user', example: '+584242380836' })
  telefono: string;
}
