import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  Get,
  HttpCode,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { SignupDto, SigninDto } from '../domain';
import { AuthService } from '../application/auth.service';
import { Public } from '../../../config/public.decorator';
import { User } from 'src/shared/domain/user.entity';
const pack = require('../../../../package.json');

@ApiBearerAuth()
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly _authService: AuthService) {}

  @Public()
  @Post('/signup')
  @UsePipes(ValidationPipe)
  @ApiOperation({ summary: 'User create' })
  async signup(@Body() signupDto: SignupDto): Promise<User> {
    return this._authService.signup(signupDto);
  }

  @Public()
  @Post('/signin')
  @UsePipes(ValidationPipe)
  @ApiOperation({ summary: 'User login' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
  })
  async signin(@Body() signinDto: SigninDto) {
    return this._authService.signin(signinDto);
  }

  @Public()
  @Get()
  @HttpCode(200)
  create() {
    return pack.version;
  }
}
