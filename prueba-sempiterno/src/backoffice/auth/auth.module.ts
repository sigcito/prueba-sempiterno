import { Module } from '@nestjs/common';
import { AuthController } from './controller/auth.controller';

import { ConfigService } from '../../config/config.service';
import { JwtStrategy } from './application/strategies/jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '../../config/config.module';
import { AuthService } from './application/auth.service';
import { PgUserRepository } from '../user/infraestructure/pguser.repository';
import { USER_REPOSITORY } from '../user/domain/user.repository';
import { userProviders } from '../user/infraestructure/user.providers';
import { DatabaseModule } from '../../database/database.module';
import { SharedModule } from 'src/shared/shared.module';

@Module({
  imports: [
    DatabaseModule,
    SharedModule,
    PassportModule.register({
      defaultStrategy: 'jwt',
    }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory() {
        return {
          secret: process.env.JWT_SECRET,
          signOptions: {
            expiresIn: '1d',
          },
        };
      },
    }),
  ],
  controllers: [AuthController],
  providers: [
    ...userProviders,
    AuthService,
    ConfigService,
    JwtStrategy,
    {
      useClass: PgUserRepository,
      provide: USER_REPOSITORY,
    },
  ],
  exports: [JwtStrategy, PassportModule],
})
export class AuthModule {}
