import { Module } from '@nestjs/common';

import { SucursalController } from './controller/sucursal.controller';
import { SucursalService } from './application/sucursal.service';
import { PgSucursalRepository } from './infraestructure/pgsucursal.repository';
import { SUCURSAL_REPOSITORI } from './domain/isucursal.repository';
import { DatabaseModule } from '../../database/database.module';
import { sucursalProviders } from './infraestructure/sucursal.providers';

@Module({
  imports: [DatabaseModule],
  providers: [
    ...sucursalProviders,
    SucursalService,
    {
      useClass: PgSucursalRepository,
      provide: SUCURSAL_REPOSITORI,
    },
  ],
  controllers: [SucursalController],
})
export class SucursalModule {}
