import { Injectable, Inject } from '@nestjs/common';
import { Sucursal } from '../../../shared/domain/c.sucursal.entity';
import {
  SUCURSAL_REPOSITORI,
  ISucursalRepository,
} from '../domain/isucursal.repository';
import {
  SucursalResultDto,
  SearchSucursalDto,
} from '../domain/search.sucursal.dto';

@Injectable()
export class SucursalService {
  constructor(
    @Inject(SUCURSAL_REPOSITORI)
    private readonly _SucursalRepository: ISucursalRepository,
  ) {}

  async get(id: string): Promise<Sucursal> {
    return this._SucursalRepository.findOne(id);
  }

  async getAll(query: SearchSucursalDto): Promise<SucursalResultDto> {
    return this._SucursalRepository.getAll(query);
  }

  async create(_Sucursal: Partial<Sucursal>): Promise<Sucursal> {
    return this._SucursalRepository.save(_Sucursal);
  }

  async update(id: string, _Sucursal: Partial<Sucursal>): Promise<Sucursal> {
    return this._SucursalRepository.update(id, _Sucursal);
  }

  async delete(id: string): Promise<boolean> {
    return this._SucursalRepository.delete(id);
  }
}
