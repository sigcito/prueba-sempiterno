import { Sucursal } from '../../../shared/domain/c.sucursal.entity';
import { DataSource } from 'typeorm';
export const sucursalProviders = [
  {
    provide: 'SUCURSAL_REPOSITORY_PROVIDERS',
    useFactory: (dataSource: DataSource) => dataSource.getRepository(Sucursal),
    inject: ['DATA_SOURCE'],
  },
];
