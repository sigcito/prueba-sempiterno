import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  Logger,
} from '@nestjs/common';

import { ISucursalRepository } from '../domain/isucursal.repository';
import { Sucursal } from '../../../shared/domain/c.sucursal.entity';
import {
  SearchSucursalDto,
  SucursalResultDto,
} from '../domain/search.sucursal.dto';
import { Repository } from 'typeorm';
import { STATUS_DELETE } from '../../../shared/constants';

@Injectable()
export class PgSucursalRepository implements ISucursalRepository {
  private readonly logger = new Logger(PgSucursalRepository.name);
  constructor(
    @Inject('SUCURSAL_REPOSITORY_PROVIDERS')
    private _roleSucursalRepository: Repository<Sucursal>,
  ) {}

  async save(save: Partial<Sucursal>): Promise<Sucursal> {
    return this._roleSucursalRepository.save(save);
  }

  async findOne(Id: string): Promise<Sucursal> {
    const sucursal: Sucursal = await this._roleSucursalRepository.findOneBy({
      id: Id,
    });
    return sucursal;
  }

  async update(Id: string, _Sucursal: Sucursal): Promise<Sucursal> {
    const info = await this._roleSucursalRepository.update(Id, _Sucursal);
    if (info.affected) {
      return this.findOne(Id);
    }
    throw new HttpException('User not found', HttpStatus.NOT_IMPLEMENTED);
  }

  async getAll(query: SearchSucursalDto): Promise<SucursalResultDto> {
    const _buscador =
      typeof query.buscado !== 'undefined'
        ? query.buscado.toLocaleString()
        : '';
    const _Query = this._roleSucursalRepository
      .createQueryBuilder('o')
      .where(
        `CONCAT(COALESCE(o.nombre,''),' ',COALESCE(o.direccion,''),' ',COALESCE(o.razon_social,'')) like '%${_buscador}%'`,
      );
    typeof query.estado !== 'undefined'
      ? _Query.andWhere('o.estado = :estado', {
          estado: query.estado,
        })
      : _Query.andWhere('o.estado != :estado', { estado: STATUS_DELETE });

    const _QueryResult = _Query;
    _QueryResult.orderBy('o.nombre', 'DESC');
    if (query.page && query.take) {
      _QueryResult.skip((query.page - 1) * 10);
      _QueryResult.take(query.take);
    }

    const result = await _QueryResult.getMany();
    const count = await _Query.getCount();

    return new SucursalResultDto(result, count, query.page);
  }

  async delete(Id: string): Promise<boolean> {
    const infor = await this._roleSucursalRepository.update(Id, {
      estado: STATUS_DELETE,
    });

    if (!infor.affected) {
      this.logger.error('NO se actualizo');
      return false;
    }
    return true;
  }
}
