import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Patch,
  Delete,
} from '@nestjs/common';

import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SucursalService } from '../application/sucursal.service';
import { Sucursal } from '../../../shared/domain/c.sucursal.entity';
import {
  SucursalResultDto,
  SearchSucursalDto,
} from '../domain/search.sucursal.dto';
import { Usersesion } from '../../../backoffice/auth/application/strategies/usersesion.decorator';

@ApiBearerAuth('acces-token')
@ApiTags('Sucursal')
@Controller('sucursal')
export class SucursalController {
  constructor(private readonly _SucursalService: SucursalService) {}

  @Get(':id')
  get(@Param('id') id: string): Promise<Sucursal> {
    return this._SucursalService.get(id);
  }

  @Post('getAll')
  getAll(@Body() query: SearchSucursalDto): Promise<SucursalResultDto> {
    return this._SucursalService.getAll(query);
  }

  @Post()
  create(@Body() body: Sucursal, @Usersesion() user): Promise<Sucursal> {
    body.username = user.username;
    return this._SucursalService.create(body);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() _Sucursal: Sucursal,
    @Usersesion() user,
  ): Promise<Sucursal> {
    _Sucursal.username = user.username;
    return this._SucursalService.update(id, _Sucursal);
  }

  @Delete(':id')
  delete(@Param('id') id: string): Promise<boolean> {
    return this._SucursalService.delete(id);
  }
}
