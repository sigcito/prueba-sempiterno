import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { Sucursal } from '../../../shared/domain/c.sucursal.entity';
export class SearchSucursalDto {
  @IsString()
  @IsOptional()
  @ApiProperty({ description: 'Optional ' })
  buscado: string;

  @IsNumber()
  @IsOptional()
  @ApiProperty({ description: 'Optional 0,1 ' })
  estado: number;

  @IsNumber()
  @IsOptional()
  @ApiProperty({ description: 'Required ', example: 1 })
  page: number;

  @IsNumber()
  @IsOptional()
  @ApiProperty({ description: 'Required ', example: 8 })
  take: number;
}

export class SucursalResultDto {
  list: Sucursal[];
  count: number;
  page: number;

  constructor(list: Sucursal[], count: number, page: number) {
    this.list = list;
    this.count = count;
    this.page = page;
  }
}
