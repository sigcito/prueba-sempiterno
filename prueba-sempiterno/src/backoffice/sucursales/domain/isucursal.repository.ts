import { Sucursal } from '../../../shared/domain/c.sucursal.entity';
import { SearchSucursalDto, SucursalResultDto } from './search.sucursal.dto';

export const SUCURSAL_REPOSITORI = 'SUCURSAL_REPOSITORI';

export interface ISucursalRepository {
  save(save: Partial<Sucursal>): Promise<Sucursal>;
  findOne(Id: string): Promise<Sucursal>;
  update(Id: string, _Sucursal: Partial<Sucursal>): Promise<Sucursal>;
  getAll(query: SearchSucursalDto): Promise<SucursalResultDto>;
  delete(Id: string): Promise<boolean>;
}
