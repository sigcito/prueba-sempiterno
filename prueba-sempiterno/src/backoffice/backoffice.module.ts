import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { ImageModule } from './image/image.module';
import { AuthModule } from './auth/auth.module';
import { SucursalModule } from './sucursales/sucursal.module';

@Module({
  imports: [UserModule, ImageModule, AuthModule, SucursalModule],
  controllers: [],
  providers: [],
})
export class BackofficeModule {}
