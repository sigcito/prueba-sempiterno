import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class SaveImageDto {
  @ApiProperty({
    description: 'URL de una imagen',
    example:
      'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
  })
  @IsNotEmpty()
  @IsString()
  url: string;
}

export class SaveImageResult {
  resultado: string;
}
