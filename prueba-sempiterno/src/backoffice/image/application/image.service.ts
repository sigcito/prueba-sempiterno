import { Injectable, Logger } from '@nestjs/common';
import { SaveImageDto, SaveImageResult } from '../domain/index';
import axios from 'axios';
import * as fs from 'fs-extra';
import * as path from 'path';

@Injectable()
export class ImageService {
  private readonly logger = new Logger(ImageService.name);
  constructor() {
    /*sonarqube*/
  }

  async saveImage(body: SaveImageDto): Promise<SaveImageResult> {
    try {
      const response = await axios.get(body.url, {
        responseType: 'arraybuffer',
      });
      const buffer = Buffer.from(response.data, 'binary');
      const filename = path.basename(body.url);

      await fs.writeFile(`C:\\image\\${filename}`, buffer);
      return { resultado: `Imagen guardada con éxito en ${filename}` };
    } catch (error) {
      console.error(`Error al guardar la imagen: ${error.message}`);
      throw new Error(`Error al guardar la imagen: ${error.message}`);
    }
  }
}
