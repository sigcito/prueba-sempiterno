import { Module } from '@nestjs/common';
import { ImageService } from './application/image.service';
import { ImageController } from './controller/image.controller';
import { DatabaseModule } from '../../database/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [ImageService],
  controllers: [ImageController],
})
export class ImageModule {}
