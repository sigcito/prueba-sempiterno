import { Body, Controller, Logger, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ImageService } from '../application/image.service';
import { SaveImageDto, SaveImageResult } from '../domain/index';

@ApiBearerAuth('acces-token')
@ApiTags('Image')
@Controller('image')
export class ImageController {
  private readonly logger = new Logger(ImageController.name);
  constructor(private readonly imageService: ImageService) {}

  @Post('saveimage')
  saveImage(@Body() body: SaveImageDto): Promise<SaveImageResult> {
    return this.imageService.saveImage(body);
  }
}
