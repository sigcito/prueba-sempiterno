import { Repository } from 'typeorm';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
  Inject,
  Logger,
} from '@nestjs/common';
import { UserRepository } from '../domain/user.repository';
import { User } from '../../../shared/domain/user.entity';
import { STATUS_ACTIVE, STATUS_DELETE } from '../../../shared/constants';
import { SearchUserDto, UserResultDto } from '../domain/search.user.dto';
import { ReadUserDto, UpdateUserDto } from '../domain';
import { plainToClass } from 'class-transformer';

@Injectable()
export class PgUserRepository implements UserRepository {
  private readonly logger = new Logger(PgUserRepository.name);
  constructor(
    @Inject('USER_REPOSITORY_PROVIDER')
    private _userRepository: Repository<User>,
  ) {}

  async validarUsuaro(username: string): Promise<User> {
    return this._userRepository.findOne({
      where: { username, status: STATUS_ACTIVE },
    });
  }

  async userExists(username: string, email: string): Promise<User> {
    return this._userRepository.findOne({
      where: [{ username }, { email }],
    });
  }

  async findOne(username: string): Promise<User> {
    return this._userRepository.findOne({
      where: { username },
    });
  }

  async save(user: User): Promise<User> {
    return this._userRepository.save(user);
  }

  async getUser(id: string): Promise<ReadUserDto> {
    if (!id) {
      throw new BadRequestException('id must be sent');
    }
    const user = await this._userRepository.findOneBy({
      status: STATUS_ACTIVE,
      id: id,
    });

    if (!user) {
      throw new NotFoundException();
    }

    return plainToClass(ReadUserDto, user);
  }

  async update(userId: string, user: Partial<UpdateUserDto>): Promise<User> {
    const foundUser = await this._userRepository.findOneBy({
      id: userId,
    });

    if (!foundUser) {
      throw new NotFoundException('User does not exists');
    }

    user.id = userId;

    const updatedUser = await this._userRepository.save(user);

    return updatedUser;
  }

  async delete(id: string): Promise<User> {
    const userExiste = await this._userRepository.findOneBy({
      id: id,
    });

    if (!userExiste) {
      throw new NotFoundException();
    }
    await this._userRepository.update(id, { status: STATUS_DELETE });

    return this._userRepository.findOneBy({ id: id });
  }

  async getAll(filter: SearchUserDto): Promise<UserResultDto> {
    const _Query = this._userRepository
      .createQueryBuilder('user')
      .andWhere(
        (filter.username ? `user.username ilike :username ` : '1=1') +
          ' and ' +
          `user.status = 1`,
        {
          username: `%${filter.username}%`,
        },
      );

    const _QueryResult = _Query;
    _QueryResult.orderBy('user.id', 'DESC');
    _QueryResult.skip((filter.page - 1) * filter.take);
    _QueryResult.take(filter.take);

    return {
      users: await _QueryResult.getMany(),
      count: await _QueryResult.getCount(),
      page: filter.page,
    };
  }
}
