import { User } from '../../../shared/domain/user.entity';
import { DataSource } from 'typeorm';
export const userProviders = [
  {
    provide: 'USER_REPOSITORY_PROVIDER',
    useFactory: (dataSource: DataSource) => dataSource.getRepository(User),
    inject: ['DATA_SOURCE'],
  },
];
