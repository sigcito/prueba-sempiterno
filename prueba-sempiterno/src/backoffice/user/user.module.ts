import { Module } from '@nestjs/common';
import { UserService } from './application/user.service';
import { UserController } from './controller/user.controller';
import { USER_REPOSITORY } from './domain/user.repository';
import { PgUserRepository } from './infraestructure/pguser.repository';
import { userProviders } from './infraestructure/user.providers';
import { DatabaseModule } from '../../database/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [
    ...userProviders,
    UserService,
    {
      useClass: PgUserRepository,
      provide: USER_REPOSITORY,
    },
  ],
  controllers: [UserController],
})
export class UserModule {}
