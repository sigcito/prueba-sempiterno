import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { User } from '../../../shared/domain/user.entity';
export class SearchUserDto {
  @ApiProperty({ description: 'Optional ' })
  @IsOptional()
  @IsString()
  username: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ description: 'Required ', example: 1 })
  page: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ description: 'Required ', example: 1 })
  take: number;
}

export class UserResultDto {
  users: User[];
  count: number;
  page: number;
}
