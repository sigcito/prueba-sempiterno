import { User } from '../../../shared/domain/user.entity';
import { ReadUserDto } from './read-user-dto';
import { SearchUserDto, UserResultDto } from './search.user.dto';
import { UpdateUserDto } from './update-user.dto';

export const USER_REPOSITORY = 'USER_REPOSITORY';

export interface UserRepository {
  getAll(query: SearchUserDto): Promise<UserResultDto>;
  getUser(id: string): Promise<ReadUserDto>;
  update(userId: string, user: Partial<UpdateUserDto>): Promise<User>;
  delete(id: string): Promise<User>;
  save(user: User): Promise<User>;
  findOne(username: string): Promise<User>;
  userExists(username: string, email: string): Promise<User>;
  validarUsuaro(username: string): Promise<User>;
}
