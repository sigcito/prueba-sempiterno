import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsDate,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateUserDto {
  @IsOptional()
  @IsNumber()
  id: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'User name', example: 'epadilla1' })
  // @ApiProperty({ example: "usename", description: 'User name' })
  readonly username: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'User email', example: 'epadilla1@gmail.com' })
  readonly email: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'User password', example: '12345' })
  readonly password: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'User currency', example: 'CPL' })
  readonly currency: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'Name of the user', example: 'Pedrito' })
  readonly nombre: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'Apellido of the user', example: 'Perez' })
  readonly apellidoPat: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'Apellido of the user', example: 'Hernandez' })
  readonly apellidoMat: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'RUT of the user', example: '6165156' })
  readonly rut: string;

  @IsOptional()
  @IsDate()
  @Type(() => Date)
  @ApiProperty({ description: 'BirthDate of the user', example: new Date() })
  readonly fechaNac: Date;

  @IsOptional()
  @IsNumber()
  @ApiProperty({ description: 'Commission of the user', example: 10 })
  readonly comision: number;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'Phone of the user', example: '5615159159' })
  readonly telefono: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'RUT of the user', example: null })
  urlImage: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ description: 'Required' })
  usernamelog: string;
}
