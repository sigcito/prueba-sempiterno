import { IsNumber, IsEmail } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { User } from 'src/shared/domain/user.entity';

@Exclude()
export class ReadUserDto {
  @Expose()
  @IsNumber()
  readonly id: string;

  @Expose()
  @IsEmail()
  readonly email: string;

  @Expose()
  @IsEmail()
  readonly username: string;
}

export class ResultGetAll {
  total: number;
  page: number;
  list: User[];
}
