import { Inject, Injectable, Logger } from '@nestjs/common';
import { User } from '../../../shared/domain/user.entity';

import { ReadUserDto, UpdateUserDto } from '../domain';
import { ResultGetAll } from '../domain/read-user-dto';
import { SearchUserDto } from '../domain/search.user.dto';
import { UserRepository, USER_REPOSITORY } from '../domain/user.repository';

@Injectable()
export class UserService {
  private readonly logger = new Logger(UserService.name);
  constructor(
    @Inject(USER_REPOSITORY)
    private readonly _userRepository: UserRepository,
  ) {}

  async getUser(id: string): Promise<ReadUserDto> {
    return this._userRepository.getUser(id);
  }

  async getAll(query: SearchUserDto): Promise<ResultGetAll> {
    const { users, count, page } = await this._userRepository.getAll(query);
    const result = new ResultGetAll();
    result.total = count;
    result.page = page;
    result.list = users;
    return result;
  }

  async update(userId: string, user: Partial<UpdateUserDto>): Promise<User> {
    return this._userRepository.update(userId, user);
  }

  async delete(id: string): Promise<User> {
    return this._userRepository.delete(id);
  }
}
