import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  ParseIntPipe,
  Patch,
  Post,
} from '@nestjs/common';

import { ReadUserDto, UpdateUserDto } from '../domain';
import { SearchUserDto } from '../domain/search.user.dto';
import { ResultGetAll } from '../domain/read-user-dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UserService } from '../application/user.service';
import { User } from 'src/shared/domain/user.entity';

@ApiBearerAuth('acces-token')
@ApiTags('Users')
@Controller('users')
export class UserController {
  private readonly logger = new Logger(UserController.name);
  constructor(private readonly _userService: UserService) {}

  @Get(':userId')
  getUser(@Param('userId') userId: string): Promise<ReadUserDto> {
    return this._userService.getUser(userId);
  }

  @Post('getAll')
  getUsers(@Body() body: SearchUserDto): Promise<ResultGetAll> {
    return this._userService.getAll(body);
  }

  @Patch(':userId')
  updateUser(
    @Param('userId') userId: string,
    @Body() user: UpdateUserDto,
  ): Promise<User> {
    return this._userService.update(userId, user);
  }

  @Delete(':id')
  deleteUser(@Param('id') id: string): Promise<User> {
    return this._userService.delete(id);
  }
}
