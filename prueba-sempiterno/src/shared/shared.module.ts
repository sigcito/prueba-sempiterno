import { Module } from '@nestjs/common';
import { UtilsService } from './services/util.service';

@Module({
  imports: [],
  exports: [UtilsService],
  providers: [UtilsService],
})
export class SharedModule {}
