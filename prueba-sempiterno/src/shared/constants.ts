import { join } from 'path';

export const STATUS_ACTIVE = 1;
export const STATUS_INACTIVE = 0;
export const STATUS_DELETE = 2;
export const TAKE = 10;

export const DIR_UPLOAD_FILES = join(__dirname);
