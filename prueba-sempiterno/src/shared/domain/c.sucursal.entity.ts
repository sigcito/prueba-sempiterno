import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('c_sucursal')
export class Sucursal extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 50, nullable: false })
  @IsNotEmpty()
  @ApiProperty({ description: 'Required', example: 'Apilink' })
  nombre: string;

  @Column({
    type: 'varchar',
    length: 100,
  })
  @ApiProperty({ description: 'Optional', example: 'Andress Test' })
  direccion: string;

  @Column({ type: 'varchar', length: 50, name: 'razon_social' })
  @ApiProperty({ description: 'Optional', example: 'Andress S.A DE S.V' })
  razonSocial: string;

  @Column({ nullable: false, default: 1 })
  @ApiProperty({
    description:
      'Optional  [0 = nohabilitado ,1 = habilitado, 2 = eliminado] [Default 1]',
    example: 1,
    default: 1,
  })
  estado: number;

  @Column({ type: 'varchar', length: 50, nullable: false })
  username: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'update_at' })
  updatedAt: Date;
}
