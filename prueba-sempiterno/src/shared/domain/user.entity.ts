import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';

@Entity('users')
export class User extends BaseEntity {
  @Expose()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', unique: true, length: 25, nullable: false })
  username: string;

  @Column({ type: 'varchar', nullable: false })
  email: string;

  @Column({ type: 'varchar', nullable: false })
  @Exclude()
  password: string;

  @Column({ nullable: false, default: 1 })
  status: number;

  @Column({ type: 'varchar', length: 30, name: 'nombre', nullable: false })
  nombre: string;

  @Column({
    type: 'varchar',
    length: 30,
    name: 'apellidopat',
    nullable: false,
  })
  apellidoPat: string;

  @Column({
    type: 'varchar',
    length: 30,
    name: 'apellidomat',
    nullable: false,
  })
  apellidoMat: string;

  @Column({ type: 'varchar', length: 30, name: 'telefono' })
  telefono: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'update_at' })
  updatedAt: Date;
}
