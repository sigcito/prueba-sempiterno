import { Injectable } from '@nestjs/common';
import { createWriteStream } from 'fs';
import * as moment from 'moment';
import { DIR_UPLOAD_FILES } from '../constants';
import { join } from 'path';

@Injectable()
export class UtilsService {
  constructor() {
    /* Sonarqube */
  }

  saveImageB64(urlImage: string) {
    const extension = urlImage.substring(
      'data:image/'.length,
      urlImage.indexOf(';base64'),
    );
    const fecha = moment().format('YYYYMMDD');
    const hora = moment().format('HHmmss');

    const nombreDoc = 'image' + '_' + fecha + '_' + hora + '.' + extension;

    let buffer = Buffer.from(urlImage.split(',')[1], 'base64');

    let fileStream = createWriteStream(
      join(DIR_UPLOAD_FILES, 'uploadesFiles', nombreDoc),
    );
    fileStream.write(buffer);
    fileStream.end();

    return join(DIR_UPLOAD_FILES, 'uploadesFiles', nombreDoc);
  }
}
